# Run the number recognition on all the images and save to tsv file

if (basename(getwd()) == "dev") setwd("..")

library(daloscreen)

file <- "data_2023-11.tsv"

cfg <- load_cfg(c("input_2023-11-27", "input_2023-11-28_1", "input_2023-11-28_2"))
model <- get_model(cfg)
df <- get_all_values(cfg, model)
df$dt <- format(df$dt, "%Y-%m-%d %H:%M:%S")
df <- df[!is.na(df$value), ]
readr::write_tsv(df, sub(".tsv$", "_raw.tsv", file))
Zscore <- roll_Zscore(df$value, cfg$outliers$window_width)
df <- df[Zscore < cfg$outliers$zscore, ]
message("Write results in ", file.path(getwd(), file))
readr::write_tsv(df, file)
