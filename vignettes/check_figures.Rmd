---
title: "check_figures"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{check_figures}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.align = "center",
  fig.width = 7,
  fig.asp= 0.61,
  out.width = "60%"
)
```

```{r setup}
library(daloscreen)
library(imager)
library(ggplot2)

path <- c("../input_2023-07-25", "../input_2023-07-26")
#path <- c("../input_2023-07-26")
cfg <- load_cfg(path)
```

## Configuration file


## Check the crop

```{r}
files <- get_file_list(cfg)
i_test <- round(runif(1, 1, length(files)))
file_test <- files[i_test]
knitr::include_graphics(file_test)
```



```{r}
p <- display_grid(file_test, cfg)
p
```


```{r}
check_crop(file_test, cfg)
```

```{r}
df <- extract_figures(file_test, cfg, forNN = FALSE)
ggplot(df,aes(value,col=as.factor(i)))+geom_histogram(bins=30)+facet_wrap(~ i)
plot_figures(df)
```



```{r}
df2 <- extract_figures(file_test, cfg, forNN = TRUE)
ggplot(df2,aes(grad,col=as.factor(i)))+geom_histogram(bins=30)+facet_wrap(~ i)
plot_figures(df2)
```
```{r, fig.width=9, fig.asp=1}
files <- get_file_list(cfg, train = TRUE)
figures <- sapply(files, get_figures_from_file, cfg = cfg)
lB <- lapply(seq_along(files), function(i) {
  df <- extract_figures(files[i], cfg = cfg, forNN = FALSE)
  df <- df[df$i == 1, ]
  df$file_index <- i
  df$file <- files[i]
  df$figure <- substr(figures[i], 1, 1)
  df$blank <- df$figure == " "
  return(df)
})
dfB <- do.call(rbind, lB)
blank_indexes <- unique(dfB$file_index[dfB$blank])
ggplot(dfB[dfB$file_index %in% sample(blank_indexes, 12), ],aes(grad))+geom_histogram(bins=30)+facet_wrap(~ file_index)
```

```{r, fig.width=9, fig.asp=1}
blank_not_indexes <- unique(dfB$file_index[!dfB$blank])
ggplot(dfB[dfB$file_index %in% sample(blank_not_indexes, 12), ],aes(grad))+geom_histogram(bins=30)+facet_wrap(~ file_index)
```

