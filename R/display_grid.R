#' Title
#'
#' @param file
#' @param n.breaks
#'
#' @return
#' @export
#'
#' @examples
display_grid <- function(file, cfg, n.breaks = 20) {
  df <- load_df_image(file, cfg)
  ggplot(df, aes(x, y)) +
    geom_raster(aes(fill=value)) +
    scale_y_continuous(trans=scales::reverse_trans(),
                       n.breaks = n.breaks) +
    scale_x_continuous(n.breaks = n.breaks) +
    scale_fill_gradient(low="black",high="white") +
    theme(panel.grid.major = element_line(color = "red",
                                          size = 0.5,
                                          linetype = 2),
          panel.ontop = TRUE,
          panel.background = element_rect(color = NA, fill = NA))
}
