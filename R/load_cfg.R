#' Load daloscreen configuration
#'
#' @param inputs [character] optional paths to the folders containing pictures and config file.
#' If specified here, this argument overwrite the inputs parameter defined in config.yml
#' @param config [character] config set (See [config::get])
#' @param file [character] name of config file ("config.yml" by default)
#'
#' @return A config object (See [config::get])
#' @export
#'
load_cfg <- function(inputs= NULL,
                     config = Sys.getenv("R_CONFIG_ACTIVE", "default"),
                     file = "config.yml") {
  cfg <- config::get(config = config,
                         file = system.file("config.yml", package = "daloscreen"))
  if (is.null(inputs)) inputs <- cfg$inputs
  cfg$inputs <- sapply(inputs, function(input) {
    if (!R.utils::isAbsolutePath(input)) {
      workDirectory <- getwd()
      while (!dir.exists(file.path(workDirectory, input))) {
        workDirectory <- dirname(workDirectory)
        if (dirname(workDirectory) == workDirectory) {
          stop("The input path ", input, " was not found in the working directory and its parents")
        }
      }
      input <- R.utils::getAbsolutePath(input, workDirectory = workDirectory)
    }
    if (!dir.exists(input)) stop(" The input folder ", input, " does not exist")
    return(input)
  })
  files <- file.path(cfg$inputs, file)
  for(i in seq(length(cfg$inputs))) {
    cfg_user <- config::get(config = config, file = files[i])
    cfg$crops[[cfg$inputs[i]]] <- cfg_user$crop
    cfg_user$crop <- NULL
    cfg <- config::merge(cfg, cfg_user)
  }
  class(cfg) <- c("config", class(cfg))
  return(cfg)
}
