#' Get paths of image files
#'
#' @param cfg
#' @param train [logical] select the list of images:
#' - `TRUE`: labelled images ready for train set
#' - `FALSE`: images that are not labelled so not available for training
#' - `NA`: all images (by default)
#'
#' @return The paths of the files
#' @export
#'
#' @examples
get_file_list <- function(cfg, train = NA) {
  pattern <- cfg$pattern$all
  if (!is.na(train) && train) {
    pattern <- cfg$pattern$train
  }
  path <- names(cfg$crops)
  files <- list.files(path, pattern = pattern, full.names = TRUE)
  if (!is.na(train) && !train) {
    files <- files[!grepl(cfg$pattern$trained, files)]
  }
  return(files)
}
