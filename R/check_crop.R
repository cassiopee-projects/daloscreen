#' Display a canvas to check if the crop is correct for extracting figures
#'
#' @param file
#' @param cfg
#'
#' @return
#' @export
#'
#' @examples
check_crop <- function(file, cfg) {
  df <- load_df_image(file, cfg)
  crop <- cfg$crops[[dirname(attr(df, "file"))]]
  n_figures <- cfg$n_figures
  ggplot(df, aes(x, y)) +
    geom_raster(aes(fill=value)) +
    geom_hline(yintercept = c(crop$top, crop$top + crop$width * cfg$aspect_ratio), size = 2, color = "blue") +
    geom_vline(xintercept = seq(crop$left, by = crop$width, length.out = max(n_figures) + 1),
               size = 2, color = "blue") +
    scale_x_continuous(limits = crop$left + c(0, crop$width * max(n_figures))) +
    scale_y_continuous(limits = crop$top + c(0, crop$width * cfg$aspect_ratio)) +
    #                   trans=scales::reverse_trans()) +
    scale_fill_gradient(low="black",high="white") +
    theme(panel.grid.major = element_line(color = "red",
                                          size = 0.5,
                                          linetype = 2),
          panel.ontop = TRUE,
          panel.background = element_rect(color = NA, fill = NA))
}
