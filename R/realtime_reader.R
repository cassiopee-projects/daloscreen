#' Display messages with last digitalized values in real time
#'
#' You need 2 sessions of R running at the same time: one running `start_capture`
#' and another one running this function. The last captured value as well as means
#' over last time steps are displayed.
#'
#' @param path The path of captured images (See [start_capture])
#' @param windows mean values over the last time steps are computed. This parameter
#' set the number of time steps on which means are computed.
#' @param inter Watching interval in seconds
#' @param fmt [sprintf] format for displaying the result as a message
#' @param cfg Daloscreen configuration (See [load_cfg])
#' @param model Figure recognition model (See [get_model])
#'
#' @return Use for side effect. Only display messages in the terminal.
#' @export
#'
realtime_reader <- function(path,
                            windows = c(10, 15, 30, 60),
                            inter = 5,
                            fmt = paste0("Q(m3/h) = %3.1f (",
                                         paste0(windows, "ts=%3.1f", collapse = ", "),
                                         ")"),
                            cfg = load_cfg(),
                            model = get_model(cfg)) {

  old_files <- character(0)
  repeat {
    startTime <- Sys.time()
    new_files <- get_file_list(cfg)
    new_files <- tail(new_files, max(windows))
    dfV <- get_all_values(cfg, model, new_files)
    Q <- sapply(c(1, windows), function(w) {
      mean(tail(dfV$value, w))
    })
    message(do.call(sprintf, c(fmt = fmt, as.list(Q))))
    sleepTime <- startTime + inter - Sys.time()
    if (sleepTime > 0)
      Sys.sleep(sleepTime)
  }
}
