
# daloscreen: Data logger from sensor screenshots

<!-- badges: start -->
<!-- badges: end -->

The goal of `daloscreen` is to take pictures of the screen of a sensor, to make
a figure recognition and to save the values

## Installation

You can install the development version of daloscreen like so:

``` r
# install.packages("remotes")
remotes::install_git("https://forgemia.inra.fr/cassiopee-projects/daloscreen.git")
keras::install_keras()
```

## Usage

The digitalization needs 3 steps:

- screen capture
- setting of the location of the figures on the screen
- training of the neural network if necessary
- digitalization and creation of the time series

### Start the capture

To save pictures in a folder and capture an image every 1 second :

``` r
daloscreen::start_capture(path = "myDir", inter = 1)
```

### Location figures settings

One needs to create a file `config.yml` in the folder where the pictures has been
saved in order to locate the figures to process in the pictures.
Running step by step the vignette `check_figures` helps to set these parameters.

This file is filled as follow :

For example, the file is filled as follow for figures with a top left corner at
the coords (414, 415) and each figure occupying a width of 65 pixels.

```yml
default:
  crop:
    width: 65
    left: 414
    top: 445
```

### Training of the neural network

See documentation of `?mturk` and the vignette `train_model`.

### Digitalization and creation of the time series

```r
library(daloscreen)
cfg <- load_cfg()
df <- get_all_values(cfg)
str(df)
```
