test_that("get_all_values works", {
  path <- system.file("input_test", package = "daloscreen")
  cfg <- load_cfg(path)
  df <- get_all_values(cfg)
  expect_equal(get_all_values(cfg),
               readr::read_tsv(system.file("input_test/data.tsv", package = "daloscreen"),
                               show_col_types = FALSE),
               ignore_attr = TRUE)
})
